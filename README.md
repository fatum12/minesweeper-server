Серверная часть игры http://minesweeper.odd.su

Клиентская часть лежить здесь https://bitbucket.org/fatum12/minesweeper-client

## Используемые технологии

* [Node.js](https://nodejs.org)
* [SockJS](https://github.com/sockjs/sockjs-node) - WebSocket сервер
* [Sequelize](http://sequelizejs.com) - работа с базой данных
