const db = require('../src/db');
const Result = require('../src/models/result');
const Game = require('../src/game');
const gameTypeParams = require('../src/enum/gameTypeParams');

const game = new Game();

run();

async function run() {
    const limit = 100;
    let offset = 0;
    let count = 0;

    while (true) {
        console.log('offset ', offset);

        let results = await Result.findAll({
            where: {
                date: {
                    $gte: '2021-12-16',
                    $lt: '2021-12-25',
                }
            },
            limit: limit,
            offset: offset,
            order: 'id',
            raw: true,
        });

        if (!results.length) {
            break;
        }

        for (let i = 0; i < results.length; i++) {
            let r = results[i];

            let mines = JSON.parse(r.mines);
            let actions = JSON.parse(r.actions);

            const firstAct = findFirstOpen(actions);

            let found = false;
            let foundRow, foundCol;
            for (let j = 0; j < mines.length; j++) {
                if (mines[j][0] === firstAct[2] && mines[j][1] === firstAct[3]) {
                    count++;
                    console.log('found id ', r.id);
                    found = true;
                    foundRow = mines[j][0];
                    foundCol = mines[j][1];

                    break;
                }
            }

            if (found) {
                const params = gameTypeParams[r.type];

                game.nHor = params[0];
                game.nVert = params[1];
                game.nMines = params[2];
                game.type = r.type;
                game.mines = mines;
                game.cells.setSize(game.nVert, game.nHor);
                for ([row, col] of game.mines) {
                    game.cells.makeMine(row, col);
                }

                game.moveMine(foundRow, foundCol);

                const new3bv = game.calc3bv()
                console.log('old 3bv =', r.v3bv, ', new ', new3bv);

                await db.query(
                    "update results set mines = :mines, v3bv = :v3bv where id = :id",
                    {
                        type: db.QueryTypes.UPDATE,
                        replacements: {
                            mines: JSON.stringify(game.mines),
                            v3bv: new3bv,
                            id: r.id,
                        }
                    }
                );
            }
        }

        offset += limit;
    }

    console.log('total', count);
}

function findFirstOpen(actions) {
    for (let i = 0; i < actions.length; i++) {
        if (actions[i][1] === 'open') {
            return actions[i];
        }
    }
    return null;
}
