const Sequelize = require('sequelize');

const config = require('../config');
const sequelize = new Sequelize(config.get('db:database'), config.get('db:username'), config.get('db:password'), {
    host: config.get('db:host'),
    dialect: config.get('db:dialect')
});
const period = require('../src/enum/period');
const gameType = require('../src/enum/gameType');

const limit = 20;

sequelize
    .query(
        `delete from results where date < DATE_SUB(now(), interval 7 day) and id not in (
            select * from (
                ${createSubquery(period.MONTH)}
                union ${createSubquery(period.ALL)}
            ) as used_results
        )`,
        {
            replacements: {
                typeBeginner: gameType.BEGINNER,
                typeIntermediate: gameType.INTERMEDIATE,
                typeExpert: gameType.EXPERT,
                limit: limit
            }
        }
    )
    .spread((results, metadata) => {
        console.log(`Affected ${metadata.affectedRows} rows`);
    });

function createSubquery(period) {
    let periodPart = '';
    if (period) {
        periodPart = 'and date >= DATE_SUB(now(), interval ' + period + ' day)';
    }

    return '(select id from results where type = :typeBeginner ' + periodPart + ' order by result asc, date desc limit :limit)' +
        ' union (select id from results where type = :typeIntermediate ' + periodPart + ' order by result asc, date desc limit :limit)' +
        ' union (select id from results where type = :typeExpert ' + periodPart + ' order by result asc, date desc limit :limit)';
}
