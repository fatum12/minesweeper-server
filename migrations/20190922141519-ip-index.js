'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addIndex('results', ['ip']);
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeIndex('results', ['ip']);
    }
};
