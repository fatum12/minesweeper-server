'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.addIndex('results', ['type', 'date']);
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.removeIndex('results', ['type', 'date']);
    }
};
