'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn('results', 'mines', {
            type: Sequelize.TEXT
        }).then(() => queryInterface.addColumn('results', 'actions', {
            type: Sequelize.TEXT
        })).then(() => queryInterface.addColumn('results', 'open_count', {
            type: Sequelize.INTEGER.UNSIGNED,
            defaultValue: 0,
            allowNull: false
        })).then(() => queryInterface.addColumn('results', 'mark_count', {
            type: Sequelize.INTEGER.UNSIGNED,
            defaultValue: 0,
            allowNull: false
        })).then(() => queryInterface.addColumn('results', 'open_around_count', {
            type: Sequelize.INTEGER.UNSIGNED,
            defaultValue: 0,
            allowNull: false
        })).then(() => queryInterface.addColumn('results', 'v3bv', {
            type: Sequelize.INTEGER.UNSIGNED,
            defaultValue: 0,
            allowNull: false
        }));
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('results', 'mines')
            .then(() => queryInterface.removeColumn('results', 'actions'))
            .then(() => queryInterface.removeColumn('results', 'open_count'))
            .then(() => queryInterface.removeColumn('results', 'mark_count'))
            .then(() => queryInterface.removeColumn('results', 'open_around_count'))
            .then(() => queryInterface.removeColumn('results', 'v3bv'))
        ;
    }
};
