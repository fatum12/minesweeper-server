'use strict';

module.exports = {
	up: function (queryInterface, Sequelize) {
		return queryInterface.addColumn(
			'results',
			'ip',
			{
				type: Sequelize.STRING(15),
				allowNull: true
			}
		);
	},
	down: function (queryInterface, Sequelize) {
		return queryInterface.removeColumn('results', 'ip');
	}
};
