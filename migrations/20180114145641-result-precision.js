'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.sequelize.query('update results set result = result * 10;');
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.sequelize.query('update results set result = result / 10;')
    }
};
