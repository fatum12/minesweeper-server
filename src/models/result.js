const Promise = require('bluebird');
const Sequelize = require('sequelize');
const gameType = require('../enum/gameType');
const period = require('../enum/period');
const db = require('../db');

const Result = db.define('result', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING(40)
    },
    result: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
    type: {
        type: Sequelize.INTEGER
    },
    date: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW()
    },
    guest: {
        type: Sequelize.BOOLEAN,
        defaultValue: 0
    },
    ip: {
        type: Sequelize.STRING(15),
        defaultValue: null
    },
    mines: {
        type: Sequelize.TEXT,
        get() {
            return JSON.parse(this.getDataValue('mines'));
        },
        set(value) {
            this.setDataValue('mines', JSON.stringify(value));
        }
    },
    actions: {
        type: Sequelize.TEXT,
        get() {
            return JSON.parse(this.getDataValue('actions'));
        },
        set(value) {
            this.setDataValue('actions', JSON.stringify(value));
        }
    },
    openCount: {
        type: Sequelize.INTEGER.UNSIGNED,
        defaultValue: 0,
        field: 'open_count'
    },
    markCount: {
        type: Sequelize.INTEGER.UNSIGNED,
        defaultValue: 0,
        field: 'mark_count'
    },
    openAroundCount: {
        type: Sequelize.INTEGER.UNSIGNED,
        defaultValue: 0,
        field: 'open_around_count'
    },
    v3bv: {
        type: Sequelize.INTEGER.UNSIGNED,
        defaultValue: 0
    },
    clicksCount: {
        type: Sequelize.VIRTUAL,
        get() {
            return this.getDataValue('openCount') + this.getDataValue('markCount') + this.getDataValue('openAroundCount');
        }
    }
}, {
    timestamps: false,
    tableName: 'results'
});

Result.findByPeriod = function (periodValue, limit = 20) {
    const fields = 'id, name, result, type, date, guest, open_count as openCount, mark_count as markCount, open_around_count as openAroundCount, v3bv, open_count + mark_count + open_around_count as clicksCount';
    let periodPart = '';

    if (periodValue) {
        periodPart = 'and date >= DATE_SUB(now(), interval ' + periodValue + ' day)';
    }
    return db.query(
        "(select " + fields + " from results where type = :typeBeginner " + periodPart + " order by result asc, date desc limit :limit)" +
        " union all (select " + fields + " from results where type = :typeIntermediate " + periodPart + " order by result asc, date desc limit :limit)" +
        " union all (select " + fields + " from results where type = :typeExpert " + periodPart + " order by result asc, date desc limit :limit)",
        {
            type: db.QueryTypes.SELECT,
            replacements: {
                typeBeginner: gameType.BEGINNER,
                typeIntermediate: gameType.INTERMEDIATE,
                typeExpert: gameType.EXPERT,
                limit: limit
            }
        }
    );
};

Result.findAllPeriods = function (limit = 20) {
    return Promise.all([
        Result.findByPeriod(period.DAY, limit),
        Result.findByPeriod(period.WEEK, limit),
        Result.findByPeriod(period.MONTH, limit),
        Result.findByPeriod(period.ALL, limit)
    ]);
};

module.exports = Result;
