const Sequelize = require('sequelize');
const config = require('../config');
const debug = require('debug')('db');

module.exports = new Sequelize(config.get('db:database'), config.get('db:username'), config.get('db:password'), {
    host: config.get('db:host'),
    dialect: config.get('db:dialect'),
    logging: debug,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});
