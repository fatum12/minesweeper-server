const redis = require('./redis');
const Blacklist = require('./blacklist');
const RateLimit = require('ratelimit.js').RateLimit;
const Promise = require('bluebird');

RateLimit.prototype.incrAsync = Promise.promisify(RateLimit.prototype.incr);

class Antiflood {

    constructor(group, interval, limit, blockTime) {
        this.group = group;

        this.blacklist = new Blacklist(group, blockTime);
        this.limiter = new RateLimit(redis, [
            {interval: interval, limit: limit}
        ]);
    }

    check(key) {
        return this.limiter.incrAsync(this.group + ':' + key)
            .then((isRateLimited) => {
                if (isRateLimited) {
                    this.blacklist.add(key);
                }
                return isRateLimited;
            });
    }

    isBlocked(key) {
        return this.blacklist.isBlocked(key);
    }
}

module.exports = Antiflood;