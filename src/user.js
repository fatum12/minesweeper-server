const config = require('../config');
const Game = require('./game');
const utils = require('./utils');

/**
 * Игрок
 */
class User {
    /**
     * @param {SockJSConnection} conn
     */
    constructor(conn) {
        this.conn = conn;
        this.name = config.get('user:defaultName');
        this.guest = true;
        this.joined = false;

        this.game = new Game();
    }

    send(event, data = {}) {
        data.id = event;
        this.conn.write(JSON.stringify(data));
    }

    /**
     * @param {string} name
     */
    setName(name) {
        name = utils.sanitizeString(name);
        
        if (name.length > config.get('user:nameMaxLength')) {
            name = name.substr(0, config.get('user:nameMaxLength'));
        }
        
        if (name.length >= config.get('user:nameMinLength')) {
            this.name = name;
            this.guest = false;
        }
    }

    getName() {
        return this.name;
    }

    isGuest() {
        return this.guest;
    }

    toJSON() {
        return {
            id: this.getId(),
            name: this.getName(),
            guest: this.isGuest()
        };
    }

    /**
     * @return {string}
     */
    getId() {
        return this.conn.id;
    }

    getIp() {
        return this.conn.headers['x-real-ip'] ? this.conn.headers['x-real-ip'] : this.conn.remoteAddress;
    }

    disconnect() {
        this.conn.end();
    }
}

module.exports = User;
