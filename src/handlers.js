const event = require('./enum/event');

/* Обработчики событий от игрока */
module.exports = {
    [event.GAME_CREATE](user, data, server) {
        if (!('type' in data)) {
            return;
        }
        user.game.createByGameType(data.type);
    },
    [event.GAME_OPEN](user, data, server) {
        if('r' in data && 'c' in data) {
            user.game.open(parseInt(data.r), parseInt(data.c));
            server.watchManager.broadcast(user.getId(), event.WATCH_ACTION, {
                action: 'open',
                r: data.r,
                c: data.c,
            });
        }
    },
    [event.GAME_MARK](user, data, server) {
        if('r' in data && 'c' in data) {
            user.game.mark(parseInt(data.r), parseInt(data.c));
            server.watchManager.broadcast(user.getId(), event.WATCH_ACTION, {
                action: 'mark',
                r: data.r,
                c: data.c,
            });
        }
    },
    [event.GAME_OPEN_AROUND](user, data, server) {
        if('r' in data && 'c' in data) {
            user.game.openAround(parseInt(data.r), parseInt(data.c));
            server.watchManager.broadcast(user.getId(), event.WATCH_ACTION, {
                action: 'openAround',
                r: data.r,
                c: data.c,
            });
        }
    },
    /**
     * Пришел новый игрок
     * @param {User} user
     * @param {Object} data
     * @param {WebsocketServer} server
     */
    [event.GAME_JOIN](user, data, server) {
        if (user.joined) {
            return;
        }
        if (('name' in data) && data.name !== null) {
            user.setName(data.name);
        }
        user.joined = true;

        // оповещаем других игроков
        server.updateUsersList();
    },
    /**
     * @param {User} user
     * @param {Object} data
     * @param {WebsocketServer} server
     */
    [event.WATCH_START](user, data, server) {
        if (!data.pid) {
            return;
        }
        const player = server.users.get(data.pid);
        if (player) {
            server.watchManager.startWatch(user, player);
        }
    },
    /**
     * @param {User} user
     * @param {Object} data
     * @param {WebsocketServer} server
     */
    [event.WATCH_STOP](user, data, server) {
        server.watchManager.stopWatch(user.getId());
    }
};
