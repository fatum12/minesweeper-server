const config = require('../config');
const redis = require('redis');
const debug = require('debug')('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

let client = redis.createClient({
    host: config.get('redis:host'),
    port: config.get('redis:port'),
    db: config.get('redis:db')
});

client.on('error', (err) => {
    debug(err);
});

module.exports = client;