const Result = require('./models/result');

module.exports = function (app) {
    app.get('/api/results/:id', (req, res) => {
        Result.findById(req.params.id)
            .then(result => {
                if (result) {
                    let resultRaw = result.get();
                    delete resultRaw.ip;

                    res.json(resultRaw);
                } else {
                    res.status(404).send('Result not found');
                }
            });
    });
};