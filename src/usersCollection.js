/**
 * Список игроков
 */
class UsersCollection {

    constructor() {
        this.users = {};
    }

    /**
     * Добавляет игрока в коллекцию
     * @param {User} user
     */
    add(user) {
        this.users[user.getId()] = user;
    }

    /**
     * @param {string} id
     * @return {User}
     */
    get(id) {
        return this.users[id];
    }

    /**
     * @param {string} id
     * @return {boolean}
     */
    has(id) {
        return this.users.hasOwnProperty(id);
    }

    getAll() {
        return this.users;
    }

    /**
     * @param {string} id
     */
    remove(id) {
        delete this.users[id];
    }

    /**
     * @return {User[]}
     */
    getJoinedUsers() {
        let id;
        let users = [];
        
        for (id in this.users) {
            if (this.users[id].joined) {
                users.push(this.users[id]);
            }
        }
        return users;
    }

    each(callback) {
        let id;
        for (id in this.users) {
            callback(this.users[id], id);
        }
    }
}

module.exports = UsersCollection;
