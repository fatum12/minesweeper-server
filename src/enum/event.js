module.exports = Object.freeze({
    RELOAD: 'reload',

    USER_AUTH: 'auth',
    
    GAME_RESULTS: 'results',
    GAME_USERS: 'usersList',
    GAME_WIN: 'win',
    GAME_JOIN: 'join',
    GAME_CREATE: 'game:create',
    GAME_CREATE_RESULT: 'game:createResult',
    GAME_OPEN: 'game:open',
    GAME_MARK: 'game:mark',
    GAME_OPEN_AROUND: 'game:openAround',

    CHAT_HISTORY: 'chatHistory',
    CHAT_MESSAGE: 'message',

    WATCH_START: 'watch:start',
    WATCH_STOP: 'watch:stop',
    WATCH_INIT: 'watch:init',
    WATCH_ACTION: 'watch:action',
    WATCH_LEAVE: 'watch:leave',
});
