module.exports = Object.freeze({
    READY: 1,
    PLAY: 2,
    LOSE: 3,
    WIN: 4
});
