const gameType = require('./gameType');

module.exports = Object.freeze({
    [gameType.BEGINNER]: [9, 9, 10],
    [gameType.INTERMEDIATE]: [16, 16, 40],
    [gameType.EXPERT]: [30, 16, 99]
});
