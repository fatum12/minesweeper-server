module.exports = Object.freeze({
    DEFAULT: 'default',
    JOIN: 'join',
    LEAVE: 'leave',
    RESULT: 'result',
    HISTORY: 'history',
    BANNED: 'banned'
});