module.exports = Object.freeze({
    DAY: 1,
    WEEK: 7,
    MONTH: 30,
    ALL: null
});