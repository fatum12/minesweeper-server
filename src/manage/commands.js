const Result = require('../models/result');
const chat = require('../chat');
const event = require('../enum/event');

module.exports = {
    /**
     * @param {WebsocketServer} server
     */
    players(server) {
        const result = [];

        server.users.getJoinedUsers().forEach((user) => {
            result.push([user.getName(), user.getIp(), user.isGuest()]);
        });

        return result;
    },
    /**
     * @param {WebsocketServer} server
     * @param {string} ip
     * @param {number} time
     */
    mute(server, ip, time) {
        server.chatAntiflood.blacklist.add(ip, time);
        chat.clearHistory();
    },
    /**
     * @param {WebsocketServer} server
     * @param {string} ip
     */
    unmute(server, ip) {
        server.chatAntiflood.blacklist.remove(ip);
    },
    /**
     * @param {WebsocketServer} server
     * @param {string} ip
     * @param {number} time
     */
    block(server, ip, time) {
        server.blacklist.add(ip, time)
            .then(() => {
                this.disconnect(server, ip);
            });
    },
    /**
     * @param {WebsocketServer} server
     * @param {string} ip
     */
    unblock(server, ip) {
        server.blacklist.remove(ip);
    },
    /**
     * Удаляет все результаты для указанного ip
     * @param {WebsocketServer} server
     * @param {string} ip
     */
    delresults(server, ip) {
        Result.destroy({
            where: {
                ip: ip
            }
        }).then(() => {
            return server.updateResultsCache();
        }).then((results) => {
            server.broadcastResults(results);
        });
    },
    /**
     * @param {WebsocketServer} server
     * @param {string} ip
     * @param {number} time
     */
    delblock(server, ip, time) {
        this.block(server, ip, time);
        this.delresults(server, ip);
    },
    disconnect(server, ip) {
        Object.values(server.users.getAll()).forEach((user) => {
            if (user.getIp() === ip) {
                user.disconnect();
            }
        });
    },
    /**
     * Отправляет клиентам команду перезагрузить страницу.
     * Используется для принудительного релиза новой версии клиентского кода.
     * @param {WebsocketServer} server
     */
    reload(server) {
        server.broadcast(event.RELOAD);
    }
};
