const ipc = require('node-ipc');
const debug = require('debug')('manage');

const commands = require('./commands');

ipc.config.appspace = 'minesweeper.';
ipc.config.id = 'master';
ipc.config.silent = true;

module.exports = function (server) {
    ipc.serve(function () {
        ipc.server.on('message', function (data, socket) {
            debug('got a message', data);

            try {
                const command = data.shift();
                if (command in commands) {
                    ipc.server.emit(socket, 'message', commands[command](server, ...data));
                } else {
                    throw new Error('Unknown command');
                }
            } catch (e) {
                ipc.server.emit(socket, 'error', e.message);
            }
        });
    });

    debug('start ipc');
    ipc.server.start();
};