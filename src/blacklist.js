const redis = require('./redis');

const KEY_PREFIX = 'blacklist:';

class Blacklist {

    constructor(group, time) {
        this.group = group;
        this.time = time;
    }

    add(key, time = this.time) {
        const k = this.createKey(key);
        const command = redis.multi().set(k, 1);

        if (time > 0) {
            command.expire(k, time);
        }

        return command.execAsync();
    }

    remove(key) {
        return redis.delAsync(this.createKey(key));
    }

    isBlocked(key) {
        return redis.existsAsync(this.createKey(key));
    }

    createKey(key) {
        return KEY_PREFIX + this.group + ':' + key;
    }
}

module.exports = Blacklist;