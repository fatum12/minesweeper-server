const sockjs = require('sockjs');
const config = require('../config');
const debug = require('debug')('server');
const UsersCollection = require('./usersCollection');
const Antiflood = require('./antiflood');
const Blacklist = require('./blacklist');
const Result = require('./models/result');
const event = require('./enum/event');
const handlers = require('./handlers');
const gameStatus = require('./enum/gameStatus');
const User = require('./user');
const template = require('./enum/template');
const WatchManager = require('./WatchManager');

/**
 * Сервер игры
 */
class WebsocketServer {

    constructor(httpServer) {
        this.resultCache = [];
        this.updateResultsCache();
        this.users = new UsersCollection();
        // не более 20 сообщений чата за 30 секунд, блокировка на 4 часа
        this.chatAntiflood = new Antiflood('chat', 30, 20, 4 * 60 * 60);
        // общий список блокировки
        this.blacklist = new Blacklist('common', 0);
        this.queue = {};
        this.watchManager = new WatchManager();

        sockjs
            .createServer({
                log: debug,
                disable_cors: config.get('sockjs:disableCors')
            })
            .on('connection', (conn) => {
                this.onConnect(conn);

                conn.on('data', (message) => {
                    this.onData(conn, message);
                });

                conn.on('close', () => {
                    this.onDisconnect(conn);
                });
            })
            .installHandlers(httpServer, {
                prefix: config.get('sockjs:prefix')
            });
    }

    /**
     * Обрабатывает новое подключение
     * @param {SockJSConnection} conn
     */
    onConnect(conn) {
        debug('New connection: ', conn);

        let user = new User(conn);

        this.blacklist.isBlocked(user.getIp())
            .then((blocked) => {
                if (blocked) {
                    conn.end();
                    return;
                }
                this.users.add(user);

                user.send(event.USER_AUTH, {
                    userId: user.getId()
                });

                user.game
                    .on('create', () => {
                        user.send(event.GAME_CREATE_RESULT, user.game.toJSON());
                        this.watchManager.sendGameState(user);
                    })
                    .on('endGame', (status) => {
                        if (status == gameStatus.WIN) {
                            this.addResult(
                                user,
                                user.game.type,
                                user.game.getResult(),
                                user.game.exportMines(),
                                user.game.actions,
                                user.game.calc3bv()
                            );
                        }
                    })
                ;

                // отсылаем результаты игр
                user.send(event.GAME_RESULTS, {
                    results: this.resultCache
                });
                // отсылаем список игроков
                user.send(event.GAME_USERS, {
                    users: this.users.getJoinedUsers()
                });

                if (conn.id in this.queue) {
                    // обрабатываем все накопленные сообщения
                    this.queue[conn.id].forEach((message) => {
                        this.onData(conn, message);
                    });
                    delete this.queue[conn.id];
                }
            });
    }

    /**
     * Получение сообщений
     * @param {SockJSConnection} conn
     * @param {string} message
     */
    onData(conn, message) {
        debug('Incoming message: ', message);

        const connId = conn.id;
        if (!this.users.has(connId)) {
            // пока не обработано подключение накапливаем все его сообщения
            if (!(connId in this.queue)) {
                this.queue[connId] = [];
            }
            this.queue[connId].push(message);
            return;
        }

        let user = this.users.get(connId);

        try {
            // проверяем формат данных
            let data = JSON.parse(message);
            if (typeof data !== 'object' || !('id' in data)) {
                throw new Error('Wrong incoming data format');
            }

            let eventName = data.id;

            if (eventName in handlers) {
                handlers[eventName](user, data, this);
            } else {
                debug(`Unknown event: ${eventName}`);
            }
        } catch (err) {
            debug(err);
        }
    }

    /**
     * Соединение закрыто
     * @param {SockJSConnection} conn
     **/
    onDisconnect(conn) {
        debug('Disconnected: ', conn);

        const connId = conn.id;
        delete this.queue[connId];

        if (!this.users.has(connId)) {
            return;
        }

        let user = this.users.get(connId);
        this.users.remove(connId);
        if (user.joined) {
            this.updateUsersList();
        }
        this.watchManager.stopWatch(connId);
        this.watchManager.stopAllWatchers(connId);
    }

    /**
     * Отправляет данные всем игрокам
     * @param {string} eventName
     * @param {Object} data
     */
    broadcast(eventName, data = {}) {
        this.users.each((user) => {
            user.send(eventName, data);
        });
    }

    /**
     * Добавляет в базу результат игры и отправляет оповещение всем игрокам
     * @param {User} user
     * @param {number} gameType
     * @param {number} result
     * @param {Array} mines
     * @param {Array} actions
     * @param {number} v3bv
     */
    addResult(user, gameType, result, mines, actions, v3bv) {
        result = parseInt(result) || 0;

        if (result <= 0) {
            return;
        }

        let openCount = 0;
        let markCount = 0;
        let openAroundCount = 0;

        actions.forEach((action) => {
            switch (action[1]) {
                case 'open':
                    openCount++;
                    break;
                case 'mark':
                    markCount++;
                    break;
                case 'openAround':
                    openAroundCount++;
                    break;
            }
        });

        Result
            .create({
                name: user.getName(),
                result: result,
                type: gameType,
                guest: user.isGuest(),
                ip: user.getIp(),
                mines: mines,
                actions: actions,
                openCount: openCount,
                markCount: markCount,
                openAroundCount: openAroundCount,
                v3bv: v3bv
            })
            .then((newResult) => {
                this.sendChatMessage(template.RESULT, {
                    user: user.getName(),
                    userId: user.getId(),
                    result: result,
                    resultId: newResult.id,
                    gameType: gameType,
                });
                return this.updateResultsCache();
            })
            .then((results) => {
                this.broadcastResults(results);
            });
    }

    /**
     * Отправляет в чат сообщение всем игрокам
     * @param {string} templateName
     * @param {Object} data
     */
    sendChatMessage(templateName, data) {
        if (!('date' in data)) {
            data.date = new Date();
        }

        this.broadcast(event.CHAT_MESSAGE, {
            template: templateName,
            data: data
        });
    }

    /**
     * Посылает всем игрокам список вошедших пользователей
     */
    updateUsersList() {
        this.broadcast(event.GAME_USERS, {
            users: this.users.getJoinedUsers()
        });
    }

    updateResultsCache() {
        return Result
            .findAllPeriods()
            .then((results) => {
                this.resultCache = results;
                return results;
            });
    }

    broadcastResults(results) {
        this.broadcast(event.GAME_RESULTS, {
            results: results ? results : this.resultCache
        });
    }
}

module.exports = WebsocketServer;
