const LOW_CHARS = '\\x00-\\x09\\x0B\\x0C\\x0E-\\x1F\\x7F';

const sanitizeRx = new RegExp(`[${LOW_CHARS}]+`, 'g');
const urlsRx = new RegExp(`https?://[^\\s]+`, 'gi');

module.exports = {
    rand(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    capitalizeFirstLetter(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    },
    inArray(arr, item) {
        return arr.indexOf(item) !== -1;
    },
    sanitizeString(str) {
        str = str.replace(sanitizeRx, '');
        return this.removeUrls(str).trim();
    },
    // удаляет из текста ссылки
    removeUrls(str) {
        return str.replace(urlsRx, '').trim();
    }
};
