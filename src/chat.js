const redis = require('./redis');
const config = require('../config');

const KEY_HISTORY = 'history';
const STORE_LIMIT = 50;
const SHOW_LIMIT = 10;

module.exports = {
    addMessage(user, message) {
        return redis.multi()
            .lpush(KEY_HISTORY, JSON.stringify({
                name: user.getName(),
                guest: user.isGuest(),
                message: message,
                date: new Date(),
                ip: user.getIp()
            }))
            .ltrim(KEY_HISTORY, 0, STORE_LIMIT - 1)
            .execAsync();
    },
    getHistory() {
        return redis.lrangeAsync(KEY_HISTORY, 0, SHOW_LIMIT - 1)
            .then(function (messages) {
                return messages.map(function (message) {
                    let obj = JSON.parse(message);
                    delete obj.ip;
                    return obj;
                });
            });
    },
    clearHistory() {
        return redis.delAsync(KEY_HISTORY);
    }
};