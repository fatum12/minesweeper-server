const event = require('./enum/event');

module.exports = class WatchManager {

    constructor() {
        this.playerWatchers = {};
        this.watcherPlayer = {};
    }

    /**
     * Начать отслеживание действий игрока
     * @param {User} watcher
     * @param {User} player
     */
    startWatch(watcher, player) {
        const watcherId = watcher.getId();
        const playerId = player.getId();
        if (watcherId === playerId) {
            return;
        }

        this.stopWatch(watcherId);

        if (!this.playerWatchers[playerId]) {
            this.playerWatchers[playerId] = {};
        }

        this.playerWatchers[playerId][watcherId] = watcher;
        this.watcherPlayer[watcherId] = playerId;

        watcher.send(event.WATCH_INIT, exportGameState(player));
    }

    /**
     * Прекратить отслеживание игрока
     * @param {string} watcherId
     */
    stopWatch(watcherId) {
        if (this.watcherPlayer[watcherId]) {
            const playerId = this.watcherPlayer[watcherId];
            delete this.playerWatchers[playerId][watcherId];
            if (Object.keys(this.playerWatchers[playerId]).length === 0) {
                delete this.playerWatchers[playerId];
            }
            delete this.watcherPlayer[watcherId];
        }
    }

    /**
     * @param {string} playerId
     */
    stopAllWatchers(playerId) {
        if (!this.playerWatchers[playerId]) {
            return;
        }
        for (let watcherId in this.playerWatchers[playerId]) {
            this.playerWatchers[playerId][watcherId].send(event.WATCH_LEAVE);
            delete this.watcherPlayer[watcherId];
        }
        delete this.playerWatchers[playerId];
    }

    /**
     * Отправляет текущее состояние игры всем зрителям данного игрока
     * @param {User} player
     */
    sendGameState(player) {
        this.broadcast(player.getId(), event.WATCH_INIT, exportGameState(player))
    }

    broadcast(playerId, event, data = {}) {
        if (!this.playerWatchers[playerId]) {
            return;
        }
        for (let watcherId in this.playerWatchers[playerId]) {
            this.playerWatchers[playerId][watcherId].send(event, data)
        }
    }
}

/**
 * @param {User} player
 * @return {Object}
 */
function exportGameState(player) {
    const g = player.game;
    return {
        player: player,
        game: {
            type: g.type,
            openCount: g.nOpen,
            flagsCount: g.nFlags,
            status: g.status,
            firstAction: g.firstAction,
            cells: g.cells,
            startTime: g.startTime,
            endTime: g.endTime,
        },
    }
}
