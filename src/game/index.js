const EventEmitter = require('events');

const Field = require('./Field');
const gameType = require('../enum/gameType');
const gameStatus = require('../enum/gameStatus');
const gameTypeParams = require('../enum/gameTypeParams');
const utils = require('../utils');

class Game extends EventEmitter {
    
    constructor() {
        super();
        
        this.nHor = 9;
        this.nVert = 9;
        this.nMines = 10;
        this.nOpen = 0;
        this.nFlags = 0;
        this.status = gameStatus.READY;
        this.firstAction = true;
        this.cells = new Field();
        this.startTime = null;
        this.endTime = null;
        this.type = gameType.BEGINNER;

        this.actions = [];
        this.mines = [];
    }

    // создает новую игру
    create(hor, vert, mines) {
        hor = parseInt(hor) || 0;
        if (hor > 0) {
            if (hor < 6) {
                hor = 6;
            }
            this.nHor = hor;
        }

        vert = parseInt(vert) || 0;
        if (vert > 0) {
            if (vert < 6) {
                vert = 6;
            }
            this.nVert = vert;
        }

        mines = parseInt(mines) || 0;
        if (mines > 0) {
            if (mines < 3) {
                mines = 3;
            }
            // заполнение минами не более 80%
            if (mines > this.nVert * this.nHor * 0.8) {
                mines = Math.floor(this.nVert * this.nHor * 0.8);
            }
            this.nMines = mines;
        }

        this.type = determineGameType(this.nHor, this.nVert, this.nMines);

        this.nOpen = 0;
        this.nFlags = 0;
        this.firstAction = true;
        this.startTime = null;
        this.endTime = null;

        this.actions = [];

        this.cells.setSize(this.nVert, this.nHor);

        // заполняем поле минами
        let n = 0;	// количество мин
        let row, col;
        this.mines = [];
        do {
            row = utils.rand(1, this.nVert);
            col = utils.rand(1, this.nHor);
            if (!this.cells.isMine(row, col)) {
                this.cells.makeMine(row, col);
                n++;
                this.mines.push([row, col]);
            }
        } while (n < this.nMines);

        this.status = gameStatus.PLAY;

        this.emit('create', this.type, this.nHor, this.nVert, this.nMines);
    }

    createByGameType(type) {
        if (type in gameTypeParams) {
            this.create(...gameTypeParams[type]);
        } else {
            this.create();
        }
    }

    endGame() {
        this.endTime = new Date();
        this.emit('endGame', this.status);

        // делаем все мины видимыми
        for (let [row, col] of this.mines) {
            this.cells.makeOpen(row, col);
        }

        this.status = gameStatus.READY;
    }

    // открывает ячейку
    open(row, col) {
        if (!this.canAct(row, col)) {
            return;
        }
        this.addAction('open', row, col);

        if (this.firstAction) {
            // если это первый клик
            if (this.cells.isMine(row, col)) {
                // и попали на мину - переносим мину
                this.moveMine(row, col);
            }
            // запускаем таймер
            this.startTime = new Date();
            this.firstAction = false;
        }
        if (this.cells.isOpen(row, col) || this.cells.isFlag(row, col)) {
            return;
        }
        if (this.cells.isMine(row, col)) {
            // игрок подорвался
            this.cells.makeFail(row, col);
            this.status = gameStatus.LOSE;
            this.endGame();
        } else {
            this.openCells(row, col);

            if (this.isWin()) {
                this.status = gameStatus.WIN;
                this.endGame();
            }
        }
    }

    // ставит флаг, вопрос
    mark(row, col) {
        if (!this.canAct(row, col)) {
            return;
        }
        this.addAction('mark', row, col);

        if (this.cells.isOpen(row, col)) {
            return;
        }

        if (this.cells.isFlag(row, col)) {
            this.cells.makeQuestion(row, col);    // сделать вопросом
            this.cells.resetFlag(row, col);       // сбросить флаг
            this.setFlagCount(-1);
        }
        else if (this.cells.isQuestion(row, col)) {
            this.cells.resetQuestion(row, col);   // сбросить вопрос
        }
        else {
            this.cells.makeFlag(row, col);        // сделать флагом
            this.setFlagCount(1);
        }
    }

    // открывает ячейки вокруг заданной
    openAround(row, col) {
        if (!this.canAct(row, col)) {
            return;
        }
        this.addAction('openAround', row, col);

        let number = this.cells.getNumber(row, col);
        if (this.cells.isOpen(row, col) && number > 0) {
            let flags = 0;
            let i, j;

            for (i = row - 1; i <= row + 1; i++) {
                for (j = col - 1; j <= col + 1; j++) {
                    if (i === row && j === col) {
                        continue;
                    }
                    if (!this.cells.isOpen(i, j) && this.cells.isFlag(i, j)) {
                        flags++;
                    }
                }
            }

            if (flags === number) {
                for (i = row - 1; i <= row + 1; i++) {
                    for (j = col - 1; j <= col + 1; j++) {
                        if (this.cells.isMine(i, j) && !this.cells.isFlag(i, j)) {
                            // игрок подорвался
                            this.status = gameStatus.LOSE;
                            this.endGame();
                            return;
                        }
                        this.openCells(i, j);

                        if (this.isWin()) {
                            this.status = gameStatus.WIN;
                            this.endGame();
                            return;
                        }
                    }
                }
            }
        }
    }

    setFlagCount(value) {
        this.nFlags += value;
    }

    // рекурсивная функция для открытия ячеек
    openCells(r, c) {
        if (!this.isValidCell(r, c)) {
            return;
        }
        if (this.cells.isOpen(r, c) || this.cells.isMine(r, c) || this.cells.isFlag(r, c)) {
            return;
        }
        this.cells.makeOpen(r, c);
        this.nOpen++;
        // вокруг ячейки нет мин
        if (this.cells.getNumber(r, c) === 0) {
            this.openCells(r - 1, c - 1);
            this.openCells(r - 1, c);
            this.openCells(r - 1, c + 1);
            this.openCells(r, c - 1);
            this.openCells(r, c + 1);
            this.openCells(r + 1, c - 1);
            this.openCells(r + 1, c);
            this.openCells(r + 1, c + 1);
        }
    }

    isWin() {
        return this.nOpen === this.getNeedOpenCount();
    }

    isValidCell(row, col) {
        return row >= 1 && col >= 1 && row <= this.nVert && col <= this.nHor;
    }

    canAct(row, col) {
        return this.status === gameStatus.PLAY && this.isValidCell(row, col);
    }

    getNeedOpenCount() {
        return this.nHor * this.nVert - this.nMines;
    }

    getResult() {
        if (!this.startTime) {
            return 0;
        }
        const now = this.endTime ? this.endTime : new Date();
        return Math.floor((now.getTime() - this.startTime.getTime()) / 10);
    }

    toJSON() {
        return {
            cells: this.cells
        };
    }

    exportMines() {
        return this.mines;
    }

    // Переносит мину в другую позицию
    moveMine(r, c) {
        for (let row = 1; row <= this.nVert; row++) {
            for (let col = 1; col <= this.nHor; col++) {
                if (!this.cells.isMine(row, col)) {
                    this.cells.makeMine(row, col);
                    this.cells.resetMine(r, c);

                    for (let k = 0; k < this.mines.length; k++) {
                        if (this.mines[k][0] === r && this.mines[k][1] === c) {
                            this.mines[k][0] = row;
                            this.mines[k][1] = col;
                            break;
                        }
                    }
                    return;
                }
            }
        }
    }

    addAction(action, row, col) {
        this.actions.push([new Date(), action, row, col]);
    }

    calc3bv() {
        const clone = new Game();
        clone.nHor = this.nHor;
        clone.nVert = this.nVert;
        clone.nMines = this.nMines;
        clone.mines = this.mines;
        clone.type = this.type;

        clone.cells.setSize(clone.nVert, clone.nHor);
        let row, col;
        for ([row, col] of clone.mines) {
            clone.cells.makeMine(row, col);
        }

        let result = 0;
        for (row = 1; row <= clone.nVert; row++) {
            for (col = 1; col <= clone.nHor; col++) {
                if (clone.cells.isOpen(row, col) || clone.cells.getNumber(row, col) !== 0 || clone.cells.isMine(row, col)) {
                    continue;
                }
                clone.openCells(row, col);
                result++;
            }
        }

        for (row = 1; row <= clone.nVert; row++) {
            for (col = 1; col <= clone.nHor; col++) {
                if (!clone.cells.isOpen(row, col) && !clone.cells.isMine(row, col)) {
                    result++;
                }
            }
        }

        return result;
    }
}

function determineGameType(hor, vert, mines) {
    let found = null;
    let params;

    for (let type in gameTypeParams) {
        params = gameTypeParams[type];
        if (params[0] === hor && params[1] === vert && params[2] === mines) {
            found = type;
            break;
        }
    }

    return found ? parseInt(found) : gameType.CUSTOM;
}

module.exports = Game;
