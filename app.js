const express = require('express');
const http = require('http');
const Server = require('./src/websocketServer');
const routes = require('./src/routes');
const config = require('./config');
const manage = require('./src/manage');

const app = express();
const server = http.createServer(app);

app.disable('x-powered-by');
routes(app);
app.use((req, res, next) => {
    res.status(404).send('File not found');
});

const websocket = new Server(server);
manage(websocket);

server.listen(config.get('port'), config.get('host'));